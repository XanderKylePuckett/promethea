package me.promethea;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdBase implements CommandExecutor
{
	public String commandName = "Base";
	public String permissionName = "me.promethea.PrometheaPlugin.cmdBase";
	public PrometheaPlugin plugin;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		return false;
	}

	@Override
	public String toString()
	{
		return commandName;
	}
}