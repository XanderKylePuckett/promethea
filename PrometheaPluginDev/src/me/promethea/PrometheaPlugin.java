package me.promethea;

import org.bukkit.ChatColor;
import org.bukkit.command.PluginCommand;
import org.bukkit.permissions.Permission;
import org.bukkit.plugin.java.JavaPlugin;

public class PrometheaPlugin extends JavaPlugin
{
	public PrometheaListener listener;
	
	private PluginCommand setupCommand(CmdBase command)
	{
		PluginCommand c = this.getCommand(command.commandName.toLowerCase());
		getServer().getPluginManager().addPermission(new Permission(command.permissionName));
		c.setExecutor(command);
		c.setPermission(command.permissionName);
		c.setPermissionMessage(ChatColor.DARK_RED + "You do not have permission to use this. (" + command.permissionName + ')');

		return c;
	}

	@Override
	public void onEnable()
	{
		this.setupCommand(new CmdHello(this));
		listener = new PrometheaListener(this);
	}

	@Override
	public void onDisable()
	{
		
	}
}