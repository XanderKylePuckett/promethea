package me.promethea;

import org.bukkit.event.Listener;

public class PrometheaListener implements Listener
{
	public PrometheaPlugin plugin;

	public PrometheaListener(PrometheaPlugin plugin)
	{
		this.plugin = plugin;
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}
}